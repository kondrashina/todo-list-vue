export const TODOS_LOCALSTORAGE_KEY = 'todos';
export const TODOS_SYNC_FIELDS = ['done', 'title'];
