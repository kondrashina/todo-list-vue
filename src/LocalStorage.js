const TEST_AVAILABLE_KEY = '@@localStorageAvailableKey';

export class LocalStorage {
    constructor({localStorageImpl = window.localStorage, namespace = ''} = {}) {
        this.localStorageImpl = localStorageImpl;
        this.namespace = namespace;
    }

    _isAvailable;

    isAvailable() {
        if (typeof _isAvailable !== 'undefined') {
            return this._isAvailable;
        }

        try {
            this.localStorageImpl.setItem(TEST_AVAILABLE_KEY, TEST_AVAILABLE_KEY);
            this._isAvailable = this.localStorageImpl.getItem(TEST_AVAILABLE_KEY) === TEST_AVAILABLE_KEY;

            this.removeItem(TEST_AVAILABLE_KEY);
        } catch (e) {
            this._isAvailable = false;
        }
        return this._isAvailable
    }

    normalizeKey(key) {
        return this.namespace ? `${this.namespace}:${key}` : key;
    }

    setItem(key, value) {
        key = this.normalizeKey(key);
        try {
            this.localStorageImpl.setItem(key, JSON.stringify(value));
        } catch (e) {
            this.localStorageImpl.setItem(key, undefined);
        }
    }

    getItem(key) {
        key = this.normalizeKey(key);

        try {
            return JSON.parse(this.localStorageImpl.getItem(key))
        } catch (e) {
            return undefined;
        }
    }

    removeItem(key) {
        key = this.normalizeKey(key);
        this.localStorageImpl.removeItem(key);
    }
}

export default new LocalStorage({namespace: 'myApp'});
